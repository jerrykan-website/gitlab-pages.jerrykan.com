#!/bin/bash

set -x

SCRIPT_DIR="$(cd "$(dirname "$0")"; pwd)"
OUTPUT_DIR="$(cd "$1"; pwd)"

while IFS= read -r line; do
    repo_name="$(echo "$line" | cut -d ':' -f 1)"
    repo="$(echo "$repo_name" | sed 's/\//%2F/g')"
    branch="$(echo "$line" | cut -d ':' -f 2)"
    path="$(echo "$line" | cut -d ':' -f 3)"
    output_path="$OUTPUT_DIR/$path"

    if [[ -n "$repo" && -n "$path" ]]; then
        echo "Including repo: '$repo_name' -> '$path'"
        mkdir -p "$(dirname "$output_path")"
        curl --location \
            --output /tmp/archive.zip \
            "https://gitlab.com/api/v4/projects/$repo/jobs/artifacts/$branch/download?job=pages&job_token=$CI_JOB_TOKEN"
        unzip /tmp/archive.zip -d /tmp/archive
        mv /tmp/archive/public "$output_path"
        rm -rf /tmp/archive.zip /tmp/archive
    fi
done < "$SCRIPT_DIR/repo_mappings.txt"
