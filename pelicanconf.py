#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
from os import path

AUTHOR = 'John Kristensen'
SITENAME = 'Stuff and Things'
SITEURL = ''

PATH = 'content'

DEFAULT_LANG = 'en'
TIMEZONE = 'Australia/Hobart'
DEFAULT_DATE_FORMAT = '%A, %d %B %Y'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_CATEGORY='General'
INDEX_SAVE_AS = 'blog/index.html'
ARTICLE_URL = 'blog/{date:%Y}/{date:%m}/{slug}.html'
ARTICLE_SAVE_AS = ARTICLE_URL
PAGE_URL = '{slug}.html'
PAGE_SAVE_AS = PAGE_URL
YEAR_ARCHIVE_URL = 'blog/{date:%Y}/index.html'
YEAR_ARCHIVE_SAVE_AS = YEAR_ARCHIVE_URL

# Blogroll
LINKS = None

# Social widget
SOCIAL = (
    ('Twitter', 'https://twitter.com/jerrykan'),
    ('GitHub', 'https://github.com/jerrykan'),
    ('GitLab', 'https://gitlab.com/jerrykan'),
    ('Stack Overflow', 'https://stackoverflow.com/users/396490/jerrykan'),
)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = path.join(path.dirname(__file__), 'templates', 'bootstrap-jerrykan')

STATIC_PATHS = [
    'static',
]
ARTICLE_EXCLUDES = STATIC_PATHS
EXTRA_PATH_METADATA = {
    'static/home.html': {'path': 'index.html'},
    'static/images/': {'path': 'images/'},
    'static/talks/': {'path': 'talks/'},
    'static/.well-known/': {'path': '.well-known/'},
}
