Title: Talks

Slide decks for various talks I've given over the years:

#### [Kubernetes](./talks/taslug2019-kubernetes/) (2018 - TasLUG)
A general overview of the main Kubernetes components and what these components
do when an application is deployed into a cluster.

#### [Deploying Web Applications: Reproducibility & Isolation](./talks/taslug2016-deploying-web-applications) (2016 - TasLUG)
A look at problems associated with deploying web applications and how
containers can be used to overcome these issues.

  - [README](https://gitlab.com/jerrykan-website/talks/taslug2016-deploying-web-applications/blob/master/README.txt) (contains information about running the demos)
  - [Talk Notes](https://gitlab.com/jerrykan-website/talks/taslug2016-deploying-web-applications/blob/master/talk_notes.txt)


#### [Distro Wars: Gentoo Linux](./talks/taslug2012-distro-wars-gentoo/) (2012 - TasLUG)
Quick summary of why someone may, or may not, want to run Gentoo as their
distro of choice.
