Testing Bottle Applications with Selenium
=========================================

:category: Programming
:tags: bottle, python, selenium, testing

`Selenium`_ is a really nice framework for testing web application front-ends
by automating actions through a web browser, but it also requires a web server
to be running so that the browser can interact with the web application. Most
other tests usually interact with the code directly, so this requirement can
also lead to a slight problem... how should the web server be started when
running tests?

The simplest way to run a Selenium test is to manually start up a web server
for your application and then run the tests against it, but this can get a bit
tedious after a while (especially if you keep forgetting to start the server).

`Django`_ provides a |LiveServerTestCase|_ which automates starting up a web
server to serve up your Django application, run your Selenium tests, and then
stop the server again. This is a really nice approach, but I wanted to be able
to do something similar when I am not using Django.

Last week I came across the `flask-testing`_ framework which `provides similar
functionality`__ for `Flask`_ applications. The flask-testing
:code:`LiveServerTestCase` is inspired by the Django version, but is much
simpler. Unfortunately it is also a bit specific to Flask applications.

.. __: https://github.com/jarus/flask-testing/blob/master/flask_testing/utils.py#L279

What I really wanted was a something that could be used for any WSGI compliant
web application. So I `wrote my own`__ which is loosely based on the
flask-testing version. You simply inherit from the :code:`LiveServerTestCase`
class instead of from :code:`unittest.TestCase` when creating your test class,
override the :code:`create_app()` method to return your WSGI application and,
write your tests as normal. When you run your tests it will handle starting and
stopping the web server in the background as required. I have written a very
basic example `Bottle`_ application called `bottle-selenium`_ to show it in
action.

.. __: https://github.com/jerrykan/bottle-selenium/blob/cd4622d9f0e7f4b5d7994275959bbde4386c6176/tests/liveserver.py

I originally wrote this to use with Bottle applications, mainly because they
are very simple to work with. My eventual goal is to use this for testing the
development of `Roundup`_ instances, so it should work with any WSGI compliant
web application.

**Update (22/03/2012):** The :code:`LiveServerTestCase` is now `available in its own package`__
called `wsgi-liveserver`_.

.. __: |filename|2013-03-22-wsgi-liveserver.rst


.. _Selenium: http://docs.seleniumhq.org/
.. _Django: https://www.djangoproject.com/
.. |LiveServerTestCase| replace:: :code:`LiveServerTestCase`
.. _LiveServerTestCase: https://docs.djangoproject.com/en/1.5/topics/testing/overview/#liveservertestcase
.. _flask-testing: https://github.com/jarus/flask-testing/
.. _Flask: http://flask.pocoo.org/
.. _Bottle: http://bottlepy.org/
.. _bottle-selenium: https://github.com/jerrykan/bottle-selenium
.. _Roundup: http://www.roundup-tracker.org/
.. _wsgi-liveserver: https://pypi.python.org/pypi/wsgi-liveserver/
