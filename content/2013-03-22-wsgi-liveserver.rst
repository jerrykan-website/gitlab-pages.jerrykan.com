wsgi-liveserver
===============

:category: Programming
:tags: package, python, wsgi-liveserver

Following on from my last post, I have now split the :code:`LiveServerTestCase`
out into its own Python package to make it easier to reuse in other projects. I
have called it wsgi-liveserver and it is the first Python package that I have
released. The package can be downloaded from `PyPI`_. The code can be found on
`GitHub`_ and a welcome any feedback.


.. _PyPI: https://pypi.python.org/pypi/wsgi-liveserver/
.. _GitHub: https://github.com/jerrykan/wsgi-liveserver
