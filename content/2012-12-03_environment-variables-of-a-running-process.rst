Environment Variables of a Running Process
==========================================

:category: SysAdmin
:tags: env, proc, cat, tr

When creating init scripts or trying to debug services on Linux it can be handy
to know what the environment variables are for a running process.  It turns out
that you can retrieve these variables from :code:`/proc` (along with lots of
other rather useful information).  The environment variables are located in
:code:`/proc/$PID/environ` where :code:`$PID` is the ID of the process we are
interested in.

:code:`cat` can be used to print out the environment variables, but the entries
are separated by null characters which makes them a bit difficult to read.  To
view the entries in a slightly more legible form we can pipe the output through
:code:`tr` to replace the null characters with new line characters:

.. code-block:: sh

    cat /proc/$PID/environ | tr '\000' '\n'

----

References: 

 * :code:`man proc`
 * Server Fault: `Environment variables of a running process on Unix?`__

.. __:  http://serverfault.com/questions/66363/environment-variables-of-a-running-process-on-unix
