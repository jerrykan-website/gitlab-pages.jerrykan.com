Testing Exception Messages
==========================

:category: Programming
:tags: python, testing

The Python |unittest|_ module provides support for testing that an exception is
raised using the |assertRaises()|_ method, but sometime we need to also test
that the exception message is what is expected. Python v2.7 introduced the
|assertRaisesRegexp()| method which can be used to test exception messages
using regular expressions, but if you are stuck with v2.6 or earlier you will
need to do something like:

.. code-block:: python

    import unittest


    def raise_exception(yup=True):
        if yup:
            raise ValueError('Yup, exception raised.') 


    class BasicExceptionTest(unittest.TestCase):
        def test_message(self):
            try:
                raise_exception(True)
                self.fail()
            except ValueError as e:
                self.assertEqual(str(e), 'Yup, exception raised.')


    if __name__ == '__main__':
        unittest.main(verbosity=2)

Looking at :code:`test_message()` we first wrap the function we are testing
(:code:`raise_exception()`) in a :code:`try` ... :code:`except` statement to
catch any exception that may be raised. If no exception is raised then we call
:code:`fail()` to signal that the test has failed. If the correct exception has
been raised (in this case :code:`ValueError`) we use :code:`assertEqual()` to
test that the exception message is correct. If an exception that we were not
expecting is raised, then it will be handled by the :code:`TestCase` class and
the test will be marked as having an error. With this simple test pattern every
possible outcome should be handled correctly.

If you plan to be writing a lot of these sorts of tests, then it may be worth
creating your own :code:`TestCase` class that provides an assert method for
testing exception messages:

.. code-block:: python

    import unittest


    def raise_exception(yup=True):
        if yup:
            raise ValueError('Yup, exception raised.') 


    class ExceptionMessageTestCase(unittest.TestCase):
        def assertRaisesMessage(self, exception, msg, func, *args, **kwargs):
            try:
                func(*args, **kwargs)
                self.fail()
            except exception as e:
                self.assertEqual(str(e), msg)


    class MessageExceptionTest(ExceptionMessageTestCase):
        def test_message(self):
            self.assertRaisesMessage(ValueError, 'Yup, exception raised.',
                                     raise_exception, True)


    if __name__ == '__main__':
        unittest.main(verbosity=2)

The :code:`assertRaisesMessage()` method is very similar to the
|assertRaises()|_ method except that it also takes a :code:`msg` argument that
will be used to compare against the exception message.

Both of these test patterns could also be extended to include the ability to
use regular expression to test messages (similar to |assertRaisesRegexp()|_),
but I generally find that simple string comparisons are usually enough for my
needs.


.. |unittest| replace:: :code:`unittest`
.. _unittest: http://docs.python.org/2/library/unittest.html
.. |assertRaises()| replace:: :code:`assertRaises()`
.. _assertRaises(): http://docs.python.org/2/library/unittest.html#unittest.TestCase.assertRaises
.. |assertRaisesRegexp()| replace:: :code:`assertRaisesRegexp()`
.. _assertRaisesRegexp(): http://docs.python.org/2/library/unittest.html#unittest.TestCase.assertRaisesRegexp
